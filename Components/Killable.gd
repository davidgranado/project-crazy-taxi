
extends Node

export var health = 100
export var maxHealth = 100
export var regenRate = 0

signal entity_dead

func _ready():
	set_process(true)

func damage(power):
	health -= power

	if health <= 0:
		get_parent().queue_free()