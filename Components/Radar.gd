
extends Area2D

var parent = null

func _ready():
	parent = get_parent()
	parent.connect("mouse_enter", self, "show")
	parent.connect("mouse_exit", self, "hide")
	

func getNearestBody():
	var nearestBody = null
	var nearestDistance = 0
	var bodiesInRange = get_overlapping_bodies()
	var currenPos = get_pos()

	for body in bodiesInRange:
		if body != parent:
			var bodyDistance = currenPos.distance_to(body.get_pos())

			if nearestBody == null or bodyDistance < nearestDistance:
				nearestBody = body
				nearestDistance = bodyDistance

	return nearestBody
