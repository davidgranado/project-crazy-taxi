extends Node

var Enemy = preload("res://Entities/Enemy.tscn")

func spawn():
	randomize()
	var enemy = Enemy.instance();
	enemy.set_pos(Vector2(100, rand_range(50, 650)))
	add_child(enemy)

func _on_Timer_timeout():
	spawn()
