extends KinematicBody2D

var speed = 100
var killable;

func _ready():
	set_process(true)
	killable = get_node("Killable")

func _process(delta):
	var pos = get_pos()
	pos.x += speed * delta
	set_pos(pos)

func damage(power):
	killable.damage(power)