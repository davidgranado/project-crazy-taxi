
extends KinematicBody2D

var radar = null
var Laser = preload("res://Entities/Laser.tscn")
var reloadTime = 1.5
var reloadTimeRemaining = 0

func _ready():
	set_process(true)
	radar = get_node("Radar")

func _process(delta):
	reloadTimeRemaining -= delta

	if reloadTimeRemaining <= 0:
		var target = radar.getNearestBody()
		
		if target:
			reloadTimeRemaining = reloadTime
			fire(target)

func fire(target):
	var newLaser = Laser.instance()
	newLaser.set_pos(self.get_pos())
	get_parent().add_child(newLaser)
	newLaser.fire(target)