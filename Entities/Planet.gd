
extends StaticBody2D

# member variables here, example:
# var a=2
# var b="textvar"

func _ready():
	get_node("Area2D").connect("body_enter", self, "_crash")

func _crash(body):
	body.queue_free()