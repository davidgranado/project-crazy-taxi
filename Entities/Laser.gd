
extends Position2D

export var power = 50

var speed = 1000

func _ready():
	pass

func fire(target):
	var tween = get_node("Tween")
	var targetPos = target.get_pos()
	var targetWr = weakref(target)
	var pos = get_pos()
	var time = pos.distance_to(target.get_pos()) / speed

	rotate(pos.angle_to_point(target.get_pos()))
	tween.interpolate_method(self, "set_pos", pos, targetPos, time, Tween.TRANS_LINEAR, Tween.TRANS_LINEAR, 0)
	tween.connect("tween_complete", self, "detonate", [targetWr])
	tween.start()
	get_node("SamplePlayer").play("laser7")

func detonate(obj, key, targetWr):
	queue_free()
	
	var target = targetWr.get_ref()

	if target:
		target.damage(power)